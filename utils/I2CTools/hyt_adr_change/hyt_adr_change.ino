// Sketch to change the address of a HYT humidity sensor
// from https://forum.arduino.cc/t/change-i2c-address-on-hyt-221-digital-humidity-sensor/253872/6

#include "Wire.h"
#define OLDADDRESS 0x28
#define NEWADDRESS 0x29
int powerPin = 5; //connect sensor VDD to pin 5

void setup()
{
  pinMode(powerPin, OUTPUT);
  Wire.begin();
  Serial.begin(57600);
}

void loop()
{
  Serial.println("Changing the address of HYT sensor");
  Serial.print("Old address (HEX): ");
  Serial.println(OLDADDRESS, HEX);
  Serial.print("New address (HEX): ");
  Serial.println(NEWADDRESS, HEX);

  Serial.println("Power-on the sensor");
  digitalWrite(powerPin, HIGH);

  Serial.println("Trying to put the sensor to Command mode");
  Wire.beginTransmission(OLDADDRESS);
  Wire.write(0xA0);        //start-command-mode
  Wire.write(0x00);        //highbyte
  Wire.write(0x00);        //lowbyte
  Wire.endTransmission();  //transmitting bytes
  Serial.println("Sensor should be now in Command mode");

  Wire.beginTransmission(OLDADDRESS);
  Wire.write(0x5C);        //change i2c address
  Wire.write(0x00);        //highbyte
  Wire.write(NEWADDRESS);  //lowbyte, new address
  Wire.endTransmission();
  Serial.println("Address should be now changed");

  Wire.beginTransmission(OLDADDRESS);
  Wire.write(0x80);        //normal mode
  Wire.write(0x00);        //highbyte
  Wire.write(0x00);        //lowbyte
  Wire.endTransmission();
  Serial.println("Sensor should be now in normal mode");

  for (uint8_t add = 0X0; add < 0X80; add++) {
    Wire.requestFrom(add, (uint8_t)1);
    if (Wire.available()) {
      Serial.print("Found I2C device from address: ");
      Serial.println(add, HEX);
    }
  }

  Serial.println("Power-off the sensor");
  digitalWrite(powerPin, LOW);

  Serial.println("Done!");
  while (1){} //infinite loop
}
