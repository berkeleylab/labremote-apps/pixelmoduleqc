# Code and scripts for Pixel Module QC

## Installation

Clone the repository

```
git clone https://gitlab.cern.ch/berkeleylab/labremote-apps/pixelmoduleqc.git
cd pixelmoduleqc
```

To operate the latest version of the cooling unit software, use branch
`juerg-dev`:

```
git checkout juerg-dev
```

Run the setup script to compile labRemote and prepare the Python environment:

```
source setup.sh
```

The first time the setup script is run, it will:

- clone a branch of `labRemote` (NOTE: to avoid conflicts with the latest
  labRemote, a labRemote branch other than `devel`, such as e.g. `juerg-dev` may
  be checked out by the `setup.sh` script)
- create the build and results directories
- cmake and compile `labRemote` and plus any subdirectories
- create and install a Python virtual environment `qcenv` with labRemote plus
  any other required Python packages (downloaded from `PyPI`)
- configure your shell to use the new `qcenv` virtual environment

After the initial installation, when `setup.sh` is run, it only sets up the
`qcenv` virtual environment.

If you don't want to run PixelModuleQC scripts and prefer to work with the
standard Python environment, you can deactivate the `qcenv` virtual environment
from your shell using

```
deactivate
```

To re-activate the virtual Python environment needed for the PixelModuleQC
scripts, run

```
source qcenv/bin/activate
```

## Updating labRemote or C programs

After making any changes to labRemote or C programs, you can update
PixelModuleQC using:

```
cd build
cmake3 -DUSE_PYTHON=on ..
make -j4
```

If you want to also rebuild the Python virtual environment:

```
deactivate
rm -rf qcenv
source setup.sh
```

To rebuild both labRemote and the Python virtual environment, you can run the
`rebuild.sh` script instead of the above two steps.

## Operating the cooling unit

The command line tool to operate the cooling unit is `coolman.py` in the
`scripts` directory.

The general steps to operate the cooling unit are:

- Update the configuration file. The default configuration file is
  `scripts/coolman.json`. Each setup has it's own section of relevant
  definitions in the configuration file. The section for the new (black foam)
  cooling unit and new electronics board is called `setup1`. The previous setup
  with the blue foam unit is called `setup0`. The default is `setup1`.
- Run the `coolman.py` script with the desired command. Options `-h` or `--help`
  show usage information.

The following examples assume you are in the `scripts` directory.

### Configuration file

The default configuration file is `coolman.json`. It contains the setup sections
for the old (`setup0`) and new (`setup1`) cooling units.

For each setup, the section

- `monitoring` defines the sensors to be monitored and the desired measurement
  interval.
- `control` defines the operating parameters for cooling and heating cycles.
  This includes the chiller operating temperature, limits for the operation of
  the Peltier elements, dew point safety margins, and PID parameters.

### To check the status of the system:

```
./coolman.py -c coolman.json -s setup1 status
```

where `-c` specifies the configuration file to use and `-s` the setup definition
within that configuration file. With the current defaults this is equivalent to

```
./coolman.py status
```

### To start continuous monitoring

```
./coolman.py -v monitor
```

With the default `coolman.json` configuration file, monitoring data will be sent
to InfluxDB. Because of the `-v` option, the data will also be logged to the
terminal (this is optional).

NOTE: Monitoring does not require the chiller or Peltier element to be switched
on, but if they are on when monitoring is started, they will be monitored and
switching them off will abort monitoring.

The monitoring data can be seen in Grafana in the dashboard corresponding to the
chosen setup, e.g. for `setup1`
[Pixel QC / Cooling Unit Setup 1](https://grafana.physics.lbl.gov:3000/d/XcCutTeMz/cooling-unit-setup1?orgId=1&refresh=5s&from=1624581563321&to=1624583363321)

### To run a single cooling or heating cycle

First prepare the cooling unit by flushing the Zarges box (about 1 SCFM on the
right side flow meter) and the module cavity (about 5 SCFH on the left side flow
meter) with dry air until the dew point in the module cavity drops to at least
-40C. To reach -55C in a reasonable amount of time, the starting dew point
should be around -45C. Turn on the Venturi vacuum pump to at least -100mbar to
ensure the module is properly pressed onto the vacuum chuck.

Then run the cooling or heating cycle using

```
./coolman.py -v operate TEMP SOAKTIME
```

where TEMP is the desired target temperature in degrees Celsius and SOAKTIME is
the time in seconds to maintain the target temperature. For example:

```
./coolman.py -v operate -55 300
```

will cool the module to -55C and leave it at that temperature for 300s before
turning off cooling. Setting SOAKTIME to -1 will continue the cooling at the set
temperature until the user aborts the program (press CTRL-C).

After the cycle is complete, the chiller will be set to the standby temperature
set in the configuration file, the Peltier element will be turned off, and
monitoring will continue until the user aborts the program by pressing CTRL-C.

### To run a predefined thermal cycling sequence

Thermal cycling sequences are defined in the configuration file in section
`cycles` separately for each setup. To run such a thermal cycling sequence, use

```angular2html
./coolman.py -v cycling NAME
```

where NAME is the name used in the config file. For example,

```angular2html
./coolman.py -v cycling standard
```

will run 10 thermal cycles from -45 C to +40 C with a soak time of 15 minutes at
each setting.
