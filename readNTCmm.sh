#!/bin/bash

# Quick and dirty script to read NTC voltage from multimeter and convert to temperature, for use in QC tools script

Vout=$(/home/eathompson/pixelmoduleqc/labRemote/build/bin/meter -e /home/eathompson/pixelmoduleqc/scripts/config_coolman_nointlck.json -n Keithley-DMM6500 -c 4 meas-voltage)

# Taken from coolman configuration
refTemp=298.15
Rref=10000
Bntc=3435
V=4.8
Rdiv=54800

Rntc=$(echo "$Rdiv * ($V / $Vout - 1)" | bc -l)
log_arg=$(echo "$Rntc / $Rref" | bc -l)
log_result=$(echo "l($log_arg)" | bc -l)
temp=$(echo "$Bntc * $refTemp / ($refTemp * $log_result + $Bntc)" | bc -l)
temp_celsius=$(echo "$temp - 273.15" | bc -l)
echo "$temp_celsius"
