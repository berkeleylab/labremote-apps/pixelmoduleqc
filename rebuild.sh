cd build
cmake3 -DUSE_PYTHON=on ..
make -j4

cd ..
deactivate
rm -rf qcenv
source setup.sh
