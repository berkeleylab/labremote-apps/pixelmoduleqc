"""
Digital sensor.
"""
import logging


class DigitalSensor:
    def __init__(self, devcomuino, digitalPin, sensorName=""):
        self.devcomuino = devcomuino
        self.digitalPin = digitalPin
        self.name = sensorName
        self.sendCommand("DGT PULLUP")

    def sendCommand(self, cmd, requireOK=True):
        """Send command and return result."""
        cmdString = "%s %i" % (cmd, self.digitalPin)
        response = self.devcomuino.sendreceive(cmdString, False)
        if requireOK and response != "OK":
            logging.error(f"Arduino command {cmdString} failed with {response}")
            raise RuntimeError()
        return response

    def status(self):
        """Return sensor status as 0 (off) or 1 (on)."""
        response = self.sendCommand("DGT READ")
        if response in ("0", "1"):
            return int(response)
        raise RuntimeError("illegal sensor status received: %s" % response)
