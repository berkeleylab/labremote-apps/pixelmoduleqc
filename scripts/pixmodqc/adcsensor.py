"""
General analog sensor read out through ADC
"""
import logging
import math


def RtoC(Rntc, refTemp, Rref, Bntc):
    if (Rntc >= 3.e+38):
        return -273.15
    absT = 1. / ((1/refTemp) + (1/Bntc) * math.log(Rntc/Rref))
    return absT - 273.15

class ADCSensor:
    def __init__(
        self,
        adc,
        adcChannel,
    ):
        self.adc = adc
        self.adcChannel = adcChannel
        self.lastValue = None

    def read(self):
        """Read ADC. Stores and returns value"""
        try:
            self.lastValue = self.adc.read(self.adcChannel)
        except Exception as e:
            logging.error("ADC failed to read voltage")
            raise RuntimeError() from e
        return self.lastValue

    def rawValue(self):
        return self.lastValue


class MPX5100_ADC(ADCSensor):
    def deltaP(self):
        """Return pressure difference in mbar
           Calibration based on MPX5100 datasheet"""
        return -10.0 * ((self.rawValue() / 5.0) - 0.04) / 0.009

class NTC_ADC(ADCSensor):
    def temperature(self):
        """Return temperature in Celsius
           Only this set of NTC and voltage divider parameters works with this class currently
           Calibration currently based on NTC datasheets"""
        T_ref = 298.15
        R_ref = 10000
        B_ntc = 3435
        Rdiv = 10000
        Vdiv = 3.3

        Rmeas = Rdiv * ((Vdiv / self.rawValue()) - 1.)
        return RtoC(Rmeas, T_ref, R_ref, B_ntc)

# TODO either add more NTC types as classes or make those params configurable
