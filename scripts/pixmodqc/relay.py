"""
Support relay connected to Arduino.
"""
import logging


class Relay:
    def __init__(self, devcomuino, digitalPin):
        self.devcomuino = devcomuino
        self.digitalPin = digitalPin

    def sendCommand(self, cmd, value=None, requireOK=True):
        """Send command and return result."""
        cmdString = "%s %i" % (cmd, self.digitalPin)
        if value is not None:
            cmdString = f"{cmdString} {value}"
        response = self.devcomuino.sendreceive(cmdString, False)
        if requireOK and response != "OK":
            logging.error(f"Arduino command {cmdString} failed with {response}")
            raise RuntimeError()
        return response

    def status(self):
        """Return relay status as 0 (off) or 1 (on)."""
        response = self.sendCommand("DGT READ", requireOK=False)
        if response in ("0", "1"):
            return int(response)
        raise RuntimeError("illegal relay status received: %s" % response)

    def setOff(self):
        """Switch relay off."""
        self.sendCommand("DGT OUT")
        self.sendCommand("DGT WRITE", "0")
        if self.status() != 0:
            raise RuntimeError("unable to switch relay at pin %i off" % self.digitalPin)

    def setOn(self):
        """Switch relay on."""
        self.sendCommand("DGT OUT")
        self.sendCommand("DGT WRITE", "1")
        if self.status() != 1:
            raise RuntimeError("unable to switch relay at pin %i on" % self.digitalPin)
