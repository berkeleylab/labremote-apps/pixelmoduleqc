"""
General analog sensor.
"""
import logging


class AnalogSensor:
    def __init__(
        self,
        devcomuino,
        adcChannel,
        sensorName="",
        supplyVoltage=5.0,
        maxAdcCounts=1023,
    ):
        self.devcomuino = devcomuino
        self.adcChannel = adcChannel
        self.name = sensorName
        self.lastValue = None
        self.supplyVoltage = supplyVoltage
        self.maxAdcCounts = maxAdcCounts
        self.adcCountPerVolt = self.maxAdcCounts / self.supplyVoltage

    def read(self):
        """Read ADC. Stores and returns value."""
        cmdString = "ADC %i" % self.adcChannel
        response = self.devcomuino.sendreceive(cmdString, False)
        try:
            self.lastValue = float(response.strip())
        except Exception as e:
            logging.error(f"Arduino command {cmdString} failed with {response}")
            raise RuntimeError() from e
        return self.lastValue

    def rawValue(self):
        return self.lastValue


class MPX5100(AnalogSensor):
    def deltaP(self):
        """Return pressure difference in mbar."""
        # Calibration based on MPX5100 data sheet
        vOut = self.rawValue() / self.adcCountPerVolt
        return -10.0 * ((vOut / 5.0) - 0.04) / 0.009


class D6FP0010(AnalogSensor):
    def flow(self):
        """Return flow in l/min."""
        # Parametrization from Matthias Saimpert
        A = 0.094003
        B = -0.564312
        C = 1.374705
        D = -1.601495
        E = 1.060657
        F = -0.269996
        vOut = self.rawValue() / self.adcCountPerVolt
        return (
            A * pow(vOut, 5)
            + B * pow(vOut, 4)
            + C * pow(vOut, 3)
            + D * pow(vOut, 2)
            + E * pow(vOut, 1)
            + F
        )


# https://media.digikey.com/pdf/Data%20Sheets/Sensirion%20PDFs/SFM3020_Preliminary_V0.4_Apr2020.pdf
class SFM3020(AnalogSensor):
    def flow(self):
        """Return flow in l/min."""
        vOut = self.rawValue() / self.adcCountPerVolt
        vDD = self.supplyVoltage
        return 212.5 * (vOut / vDD - 0.1) - 10


# https://www.mouser.com/datasheet/2/187/hwscs06478_1-2270788.pdf
class HIH4031(AnalogSensor):
    def humidity(self):
        """Return relative humidity"""
        vOut = self.rawValue() / self.adcCountPerVolt
        vDD = self.supplyVoltage
        return (1 / 0.0062) * (vOut / vDD - 0.16)


class FluidSensor(AnalogSensor):
    def status(self):
        """Return 1 if fluid detected, 0 otherwise."""
        # Calibration: a small drop of chiller liquid leads
        # to an output of >= 140 ADC counts
        return 1 if self.rawValue() > 30.0 else 0
