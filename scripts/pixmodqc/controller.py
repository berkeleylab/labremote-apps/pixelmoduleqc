"""
Controller for operating cooling unit.
"""
__author__ = "Juerg Beringer"

import asyncio
import atexit
import functools
import logging
import multiprocessing
import signal
import time
import traceback

from influxdb import InfluxDBClient
from simple_pid import PID

from pixmodqc.config import DeviceStream
from pixmodqc.heartbeat import HeartBeat

MONITORINGSTATUS = {0: "not running", 1: "running"}

WATCHDOGSTATUS = {0: "not running", 1: "running"}

CONTROLLERSTATUS = {
    0: "not running",
    1: "running but idle",
    2: "cooling",
    3: "heating",
    9: "standby after successful completion",
    10: "standby after error",
    11: "aborted after error",
    12: "non-fatal error",
    13: "unresponsive, killed by watchdog"
}

MODULESTATUS = {
    0: "not running",
    1: "running but idle",
    2: "cooling",
    3: "heating"
}

SWINTSTATUS = {
    0: "not running",
    1: "running, interlock in GOOD state (not triggered)",
    2: "running, interlock in WARNING state (not triggered)",
    3: "running, interlock in CRITICAL state (triggered)"
}

# Exceptions
class InterlockError(Exception):
    """Exception raised by interlocks."""


class HardwareError(Exception):
    """Exception raised by hardware not available or powered off."""


def coroExceptionHandler(
    awaitable,
    controller,
    exceptionsForceStandby,
    exceptionsAreFatal,
    propagateExceptions=False,
):
    """Coroutine wrapper to ensure exceptions are logged immediately.

    If exceptionsAreFatal is true, the event loop is stopped.
    If propagateExceptions is True, exceptions are raised to the event loop."""
    try:
        loop = asyncio.get_event_loop()
        if loop.is_running():
            logging.error("Cannot run syncExceptionHandler in an already running event loop")
            raise RuntimeError() from None
        return loop.run_until_complete(awaitable)
    except Exception as e:
        logging.error("%s from %s", str(e), repr(awaitable))
        if not isinstance(e, (InterlockError, HardwareError)):
            for line in traceback.format_exc().strip().split("\n"):
                logging.error(line)
        if exceptionsForceStandby:
            controller.controlError = 10
            controller.standby()
        if exceptionsAreFatal:
            logging.fatal("fatal exception - aborting")
            controller.controlError = 11
            if loop.is_running():
                loop.stop()
        else:
            if controller.controlError is None:
                controller.controlError = 12
            logging.info(
                "error is non-fatal - program execution continues for other operations such as monitoring"
            )
        if propagateExceptions:
            raise e


# TODO: fix me!
def coroSignalHandler(sigName, eventLoop):
    """Signal handler for controlled shutdown of the event loop."""
    logging.info("received signal %s", sigName)
    if sigName == "SIGHUP":
        # Ignore SIGHUP to continue, even when the controlling terminal disconnects
        pass
    else:
        # For all other signals that we catch, stop processing
        eventLoop.stop()


async def waitPeriod(lastTime, period):
    """Coroutine to wait time needed for periodic execution."""
    nextTime = lastTime + period
    await asyncio.sleep(period)
    return nextTime

class ModuleController:
    def __init__(self, config, verbosePid=False, monitorsensors=None, measurementName=None, setupname=None, pidtunings=None, client=None):
        logging.debug("initializing ModuleController with configuration %s", config.get('name'))
        self.config = config
        self.name = None
        self.verbosePid = verbosePid
        self.client = client
        self.peltierSensor = None
        self.peltierPower = None
        self.peltierHBridge = None
        self.peltierHBridgeDevice = None
        self.peltierMinVoltage = None
        self.peltierMaxVoltage = None
        self.peltierMaxCurrent = None
        self.targetTemperature = None
        self.lastPidTemperature = None
        self.rampStartTime = None
        self.rampStartTemperature = None
        self.monRampSensor = None
        self.rampStart = False
        self.rampEnabled = False
        self.rampRate = None
        self.monRampLastTime = None
        self.monRampLastTemperature = None
        self.lastPidTime = None
        self.pidRampRatePerMin = None
        self.moduleHeartbeat = None

        # Set parameters from config
        for key, value in config.items():
            setattr(self, key, value)

        self.pid = PID(
            output_limits=(
                -self.peltierMaxVoltage,
                self.peltierMaxVoltage,
            )
        )
        self.pid.tunings = (
            pidtunings["Kp"],
            pidtunings["Ki"],
            pidtunings["Kd"],
        )
        self.peltierQuery = self.makeQuery(monitorsensors, measurementName, setupname)

    def determineHeatingCooling(self, currentTemp):
        if self.targetTemperature > currentTemp:
            # Heating
            self.moduleHeartbeat.writeStatus(3)
        else:
            # Cooling
            self.moduleHeartbeat.writeStatus(2)

    def makeQuery(self, monSensorList, measurementName, setupname):
        """Creates the queries to grab data from InfluxDB"""
        peltierSensorInfo = next(filter(lambda x: x["name"] == self.peltierSensor, monSensorList), None)
        return ('SELECT T FROM "' + measurementName + '" WHERE ("Setup" = \'' + setupname + '\' AND "Quantity" = \'' + peltierSensorInfo["quantity"] + '\') ORDER BY time DESC LIMIT 1')

    # TODO push this data to influx
    def monRampRate(self):
        """Utility method and monitoring device for calculating and monitoring ramp rate."""
        if self.monRampSensor is None:
            self.monRampSensor = self.config.getDevice(self.controlSetup["monRampSensor"])
        if self.monRampLastTime is None:
            self.monRampSensor.read()
            self.monRampLastTemperature = self.monRampSensor.temperature()
            self.monRampLastTime = time.time()
            return None
        now = time.time()
        if now - self.monRampLastTime >= self.monRampInterval:
            self.monRampSensor.read()
            temperature = self.monRampSensor.temperature()
            rampRate = (
                (temperature - self.monRampLastTemperature) / (now - self.monRampLastTime) * 60.0
            )
            logging.debug("module ramp rate = %4.2f C/min", rampRate)
            self.monRampLastTime = now
            self.monRampLastTemperature = temperature
            return rampRate
        return None

    # TODO push this data to influx
    def monModStatus(self):
        """Monitoring device to monitor module status.

        See MODULESTATUS for meaning of status codes."""
        h = HeartBeat(f"{self.name}", self.config.setupName)
        try:
            lastStatus = h.readData()["status"]
        except Exception:
            lastStatus = None
        if h.status()[0] is None:
            # Controller process not running
            if lastStatus >= 10:
                return lastStatus
            return 0
        # Controller process running
        if lastStatus is None:
            return 10  # Unknown error - should never happen
        return lastStatus


    def pidStep(self, targetTemperature):
        # Read all sensors
        startTime = time.time()
        result = self.client.query(self.peltierQuery)
        currentTemperature = [float(record.get("T")) for record in result.get_points()][0]

        # Ramp rate control
        setTemperature = targetTemperature
        if self.rampStart or self.rampEnabled:
            if self.rampStart:
                self.rampStart = False
                self.rampEnabled = True
                self.rampStartTime = startTime
                self.rampStartTemperature = currentTemperature
                if targetTemperature > currentTemperature:
                    # Heating (relative to current temperature)
                    self.rampRate = self.pidRampRatePerMin / 60.0
                else:
                    # Cooling (relative to current temperature)
                    self.rampRate = -self.pidRampRatePerMin / 60.0
                if self.verbosePid:
                    logging.info(
                        "PID: starting controlled ramping with rate = %4.3f C/s",
                        self.rampRate,
                    )
            rampTemperature = (
                startTime - self.rampStartTime
            ) * self.rampRate + self.rampStartTemperature
            if (self.rampRate > 0.0 and rampTemperature < targetTemperature) or (
                self.rampRate <= 0.0 and rampTemperature > targetTemperature
            ):
                setTemperature = rampTemperature
            else:
                self.rampEnabled = False
                self.rampStartTime = None
                if self.verbosePid:
                    logging.info("PID: ending controlled ramping at %4.1f C ", currentTemperature)

        self.pid.setpoint = setTemperature
        setVoltage = self.pid(currentTemperature)
        try:
            if abs(setVoltage) < self.peltierMinVoltage:
                # FIXME: The following is a hack needed because we cannot set the voltage to less than 0.8V on the BK Precision 1688B
                self.peltierPowerDevice.turnOff()
                if self.verbosePid:
                    logging.info(
                        "PID: %4.1f C (set %4.1f), PID output %5.1f V, peltier OFF",
                        currentTemperature,
                        setTemperature,
                        setVoltage,
                    )
            else:
                if self.peltierHBridgeDevice is not None:
                    relayStatus = self.peltierHBridgeDevice.status()
                    if setVoltage < 0.0:
                        # Cooling - relay must be off
                        if relayStatus == 1:
                            logging.info("switching to cooling - turning relay off")
                            self.peltierPowerDevice.turnOff()
                            self.peltierHBridgeDevice.setOff()
                        self.peltierPowerDevice.turnOn(True)
                        self.peltierPowerDevice.setVoltageLevel(-setVoltage, True)
                    else:
                        # Heating - relay must be on
                        if relayStatus == 0:
                            logging.info("switching to heating - turning relay on")
                            self.peltierPowerDevice.turnOff()
                            self.peltierHBridgeDevice.setOn()
                        self.peltierPowerDevice.turnOn(True)
                        self.peltierPowerDevice.setVoltageLevel(setVoltage, True)
                else:
                    if setVoltage < 0.0:
                        # Cooling
                        self.peltierPowerDevice.turnOn(True)
                        self.peltierPowerDevice.setVoltageLevel(-setVoltage, True)
                    else:
                        # Heating
                        self.peltierPower.turnOff()
                if self.verbosePid:
                    logging.info(
                        "PID: %4.1f C (set %4.1f C), PID output %5.1f V, peltier at %4.2f V, %4.2f A, %s",
                        currentTemperature,
                        setTemperature,
                        setVoltage,
                        self.peltierPowerDevice.measureVoltage(),
                        self.peltierPowerDevice.measureCurrent(),
                        "HEAT" if self.peltierHBridgeDevice.status() and self.peltierHBridgeDevice is not None else "COOL",
                    )
        except Exception as e:
            logging.error("unable to control Peltier power supply - aborting")
            logging.error(e)
            raise HardwareError() from e

        # Logging of PID info and ramp rate
        if self.verbosePid:
            p, i, d = self.pid.components
            logging.info("PID components: P = %.2f, I = %.2f, D = %.2f", p, i, d)
            logging.info("PID step took %2.3f seconds", time.time() - startTime)
            try:
                lastRampRate = (
                    (currentTemperature - self.lastPidTemperature)
                    / (startTime - self.lastPidTime)
                    * 60.0
                )
            except Exception:
                lastRampRate = None
            if lastRampRate is not None:
                logging.info("PID ramp rate = %4.2f C/min", lastRampRate)
        self.lastPidTemperature = currentTemperature
        self.lastPidTime = startTime

        return currentTemperature

class CoolingUnitController:
    def __init__(self, config, verbosePid=False, multiprocessFlag=False, useasyncio=True):
        logging.debug("initializing CoolingUnitController with configuration %s", config.setupName)
        self.config = config
        self.config.controller = (
            self  # Tell config about this Controller so we can monitor it like any other device
        )
        self.verbosePid = verbosePid
        self.multiprocessFlag = multiprocessFlag
        self.client = self.loginInflux()

        # Initialize for monitoring
        self.monSetup = self.config["monitoring"]
        self.monStream = None
        self.monName = self.monSetup["measurementName"]
        self.monSensors = []

        # Initialize for controlling cooling unit
        self.controlHeartbeat = None
        self.controlError = None
        self.controlSetup = self.config["control"]
        self.coldplateSensor = None
        self.envSensor = None
        self.chiller = None
        self.interlocks = []
        self.temperatureAccuracy = self.controlSetup["temperatureAccuracy"]
        self.maxAllowedTemperature = self.controlSetup["maxAllowedTemperature"]
        self.chillerTemperatureAccuracy = self.controlSetup["chillerTemperatureAccuracy"]
        self.pidInterval = self.controlSetup["pidInterval"]
        self.chillerTemperature = None
        self.soakTime = None
        self.monRampInterval = self.controlSetup["monRampInterval"]

        if useasyncio:
            # Initialize asyncio event loop
            self.eventLoop = asyncio.get_event_loop()
            for sigName in ("SIGHUP", "SIGTERM", "SIGINT"):
                self.eventLoop.add_signal_handler(
                    getattr(signal, sigName),
                    functools.partial(coroSignalHandler, sigName, self.eventLoop),
                )

        # Register exit handler to ensure everything is properly turned off as needed upon program termination
        self.running = False
        atexit.register(self.standby)

        # Initialize ModuleControllers
        self.modules = []
        for m in self.controlSetup["moduleSetups"]:
            if not m.get('enabled'):
               logging.info(f"{m.get('name')} is not enabled - skipping")
               continue
            logging.info(f"Adding ModuleController for {m.get('name')}")
            self.modules += [ ModuleController(m, self.verbosePid, self.config["monitorsensors"], self.monSetup["measurementName"], self.config["setupname"], self.controlSetup["pidtunings"], self.client) ]

    def loginInflux(self):
        """Creates client to login to InfluxDB with given credentials"""
        credentials = self.config["datasinks"][1]
        return InfluxDBClient(credentials["host"], credentials["port"], credentials["username"], credentials["password"], credentials["database"])

    def monMonStatus(self):
        """Monitoring device for monitoring itself."""
        return 1

    def monWatchdogStatus(self):
        """Monitoring device for monitoring watchdog status."""
        if HeartBeat("watchdog", self.config.setupName).isRunning():
            return 1
        # Temporary fix to check if wdStatus is being pushed to influxDB (it is!)
        return 0

    def monSWintStatus(self):
        """Monitoring device for monitoring software interlock status."""
        swint_heartbeat = HeartBeat("software-interlock", self.config.setupName)
        try:
            lastStatus = swint_heartbeat.readData()["status"]
        except Exception as e:
            lastStatus = None
            logging.warning(f"unable to read software interlcok status, exception: {e}")
            return 0
        if not swint_heartbeat.isRunning():
            # Software interlock not running
            return 0
        if lastStatus is None:
            return 0
        return lastStatus

    def monCtrlStatus(self):
        """Monitoring device to monitor controller status.

        See CONTROLLERSTATUS for meaning of status codes."""
        h = HeartBeat("control", self.config.setupName)
        try:
            lastStatus = h.readData()["status"]
        except Exception as e:
            lastStatus = None
            logging.warning(f"unable to read controller status, exception: {e}")
            return 1
        if h.status()[0] is None:
            # Controller process not running
            if lastStatus >= 10:
                return lastStatus
            return 0
        # Controller process running
        if lastStatus is None:
            return 10  # Unknown error - should never happen
        return lastStatus

    def monitoring(self, monHeartbeat, device_group, monInterval=0.0):
        """Monitoring coroutine."""
        time.time()
        i = 1
        while True:
            logging.debug("--- starting new measurement cycle ---")
            startTime = time.time()
            for ds in device_group:
                tmpStartTime = time.time()
                if i % ds.prescale == 0:
                    ds.measure()
                logging.debug(f"Measurement took: {time.time() - tmpStartTime}")
                time.sleep(0.01)
            i += 1
            if i == 101:
                i = 1
            logging.debug(
                "measuring %i sensors took %2.3f seconds",
                len(device_group),
                time.time() - startTime,
            )
            monHeartbeat.beat()
            time.sleep(monInterval)

    def scheduleMonitoring(self, monitoringStream):
        """Prepare controller for monitoring and schedule monitoring coroutine."""
        # Create a separate monitoring task for each group of sensors
        processes = []
        debugmode = False
        if logging.root.level <= logging.DEBUG:
            debugmode = True

        allmonSensors = []
        for deviceGroup in self.monSetup["groups"]:
            if not deviceGroup["enable"]:
                continue
            monHeartbeat = HeartBeat(
                "monitoring", self.config.setupName + "_" + deviceGroup["name"]
            )
            logging.debug(monHeartbeat.name)
            monHeartbeat.acquireLock("monitoring cannot be run multiple times")
            self.monStream = monitoringStream
            monSensors = []

            for deviceName in deviceGroup["sensors"]:
                try:
                    monSensors.append(
                        DeviceStream(deviceName, self.monStream, self.monName, self.config)
                    )
                except Exception as e:
                    logging.error(
                        "unable to configure device %s for stream %s - will not monitor this device",
                        deviceName,
                        self.monStream,
                    )
                    raise RuntimeError() from e
            if not debugmode:
                process = multiprocessing.Process(
                    target=self.monitoring, args=(monHeartbeat, monSensors, deviceGroup["measurementInterval"])
                )
                process.start()
                processes.append(process)
            allmonSensors += monSensors

        if not debugmode:
            try:
                for process in processes:
                    process.join()
            except Exception:
                logging.info("Monitoring process received interrupt. Terminating processes.")
                for p in processes:
                    p.terminate()
        else:
            self.monitoring(monHeartbeat, allmonSensors)

    def prepareForControl(self):
        """Prepare controller for cooling unit control."""
        for deviceGroup in self.monSetup["groups"]:
            if not deviceGroup["enable"]:
                continue
            if not HeartBeat(
                "monitoring", self.config.setupName + "_" + deviceGroup["name"]
            ).isRunning():
                logging.error('monitoring is not running for {self.config.setupName+"_"+deviceGroup["name"])} - please start it ./coolman.py monitor')
                raise RuntimeError()
        if not HeartBeat("watchdog", self.config.setupName).isRunning():
            logging.error("watchdog is not running - please start it ./coolman.py watchdog")
            raise RuntimeError()

        self.controlHeartbeat = HeartBeat("control", self.config.setupName)
        self.controlHeartbeat.acquireLock("hardware can only be controlled by a single process")
        self.controlHeartbeat.writeStatus(1)
        self.running = True

        # Interlocks
        for interlockName in self.controlSetup["interlocks"]:
            interlockDev = self.config.getDevice(interlockName)
            self.interlocks.append(interlockDev)
            try:
                interlockDev.read()
            except Exception:
                logging.info("Could not read interlockDev")
                pass
            status = interlockDev.status()
            logging.info("interlock %-20s   status = %i", interlockName, status)
            if status != 0:
                raise InterlockError("interlock %s triggered" % interlockName)

        # Configure chiller but don't change its status
        chillerName = self.controlSetup.get("chiller")
        if chillerName is not None:
            self.chiller = self.config.getDevice(chillerName)
        else:
            logging.info("%s does not control a chiller - nothing to do", self.config.setupName)

        # Configure power supply for Peltier element but leave it off for now
        for mod in self.modules:
            logging.info(
                f"{mod.name}: configuring peltier power supply, setting to min voltage of %s V",
                mod.peltierMinVoltage,
            )
            try:
                mod.peltierPowerDevice = self.config.getDevice(mod.peltierPower)
                mod.peltierPowerDevice.turnOff()
                if mod.peltierHBridge is not None:
                    mod.peltierHBridgeDevice = self.config.getDevice(mod.peltierHBridge)
                    mod.peltierHBridgeDevice.setOff()
            except Exception as e:
                logging.error(f"Power supply for Peltier element in {mod.name} is off or not responding")
                raise HardwareError() from e
            try:
                mod.peltierPowerDevice.setVoltageLevel(mod.peltierMinVoltage, True)
                mod.peltierPowerDevice.setCurrentProtect(mod.peltierMaxCurrent, True)
                mod.peltierPowerDevice.setCurrentLevel(mod.peltierMaxCurrent, True)
            except Exception as e:
                logging.error(f"Encountered error: {e}.")
                logging.error("Shutting down system gracefully")
                self.standby()
            mod.moduleHeartbeat = HeartBeat(f"{mod.name}", self.config.setupName)
            mod.moduleHeartbeat.acquireLock("hardware can only be controlled by a single process")
            mod.moduleHeartbeat.writeStatus(1)

            # Reset PID control
            mod.pid.reset()

    async def _setChiller(self, setTemperature, settleTime=0):
        """Coroutine to set chiller to new temperature.

        If setTemperature is None, _setChiller returns without doing anything.
        If no chiller is configured, _setChiller returns without doing anything.
        If settleTime is -1, _setChiller waits until the desired temperature is reached.
        Otherwise, if settleTime is non-zero, waits for the given time for chiller to settle.
        """
        if setTemperature is None:
            logging.info("no chiller operating temperature defined - not changing chiller settings")
            return
        if self.chiller is None:
            logging.error(
                "no chiller configured - cannot set chiller operating temperature to %s C",
                setTemperature,
            )
            return
        try:
            if not self.chiller.getStatus():
                logging.info("turning on chiller")
                self.chiller.turnOn(True)
        except Exception as e:
            logging.error("chiller powered off or not responding")
            raise HardwareError() from e
        logging.info("setting chiller to %s C", setTemperature)
        self.chiller.setTargetTemperature(setTemperature, False)
        await asyncio.sleep(5)
        logging.debug(
            "chiller now at %4.1f C (target %4.1f C)",
            self.chiller.measureTemperature(),
            self.chiller.getTargetTemperature(),
        )
        if settleTime > 0:
            logging.info("waiting %i s for chiller to start up", int(settleTime))
            await asyncio.sleep(settleTime)
        elif settleTime == -1:
            currentTemperature = self.chiller.measureTemperature()
            logging.info(
                "waiting for chiller to reach target temperature of %4.1f C, currently at %4.1f C",
                setTemperature,
                currentTemperature,
            )
            while abs(currentTemperature - setTemperature) > self.chillerTemperatureAccuracy:
                await asyncio.sleep(10)
                currentTemperature = self.chiller.measureTemperature()
        logging.info(
            "chiller now at %4.1f C (target %4.1f C)",
            self.chiller.measureTemperature(),
            self.chiller.getTargetTemperature(),
        )


    async def _operate(self, setChiller=False, standbyAtEnd=True, gracefulStandby=True):
        """Coroutine for cooling/heating to and operation at target temperature."""
        if setChiller:
            await self._setChiller(
                self.controlSetup["chillerOperatingTemperature"],
                self.controlSetup["chillerSettleTime"],
            )

        for mod in self.modules:
          logging.info(f"Ramping to {mod.targetTemperature} on {mod.name}")
        targetsAchieved = [False for m in self.modules]

        # Reset currentTemperatures
        for mod in self.modules:
            mod.currentTemperature = None

        while not all(targetsAchieved):

          now = time.time()
          for i, mod in enumerate(self.modules):
            if mod.targetTemperature is None:
                logging.debug(f"{mod.name} is enabled but no target Temperature specified - skipping")
                targetsAchieved[i] = True
                continue

            if (mod.currentTemperature is None) or abs(
                mod.currentTemperature - mod.targetTemperature
            ) > self.temperatureAccuracy:
                mod.currentTemperature = mod.pidStep(mod.targetTemperature)
                logging.debug(f"CurrentTemp = {mod.currentTemperature}, target = {mod.targetTemperature}, accuracy = {self.temperatureAccuracy}")
                self.controlHeartbeat.beat()
            else:
              mod.currentTemperature = mod.pidStep(mod.targetTemperature)
              if not targetsAchieved[i]:
                logging.info(
                  f"{mod.name}: target temperature of {mod.targetTemperature} C reached (now at {mod.currentTemperature} C)")
                targetsAchieved[i] = True
          now = await waitPeriod(now, self.pidInterval)

        # All target temps achieved, start soak time
        if self.soakTime >= 0:
            logging.info("starting soaking for %4.1f s", self.soakTime)
            startSoaking = time.time()
            while time.time() < startSoaking + self.soakTime:
                for mod in self.modules:
                    if mod.targetTemperature is None:
                        logging.debug(f"{mod.name} is enabled but no target Temperature specified - skipping")
                        continue
                    currentTemperature = mod.pidStep(mod.targetTemperature)
                    self.controlHeartbeat.beat()
                now = await waitPeriod(now, self.pidInterval)
            logging.debug("end of soak time reached")
        else:
            logging.info("will keep current temperature until interrupted")
            while True:
                for mod in self.modules:
                    if mod.targetTemperature is None:
                        logging.debug(f"{mod.name} is enabled but no target Temperature specified - skipping")
                        continue
                    mod.pidStep(mod.targetTemperature)
                    self.controlHeartbeat.beat()
                now = await waitPeriod(now, self.pidInterval)

        if standbyAtEnd:
            if gracefulStandby:
                logging.info(
                    "gracefully ramping to standby temperature of %4.1f C",
                    self.controlSetup["ambientTemperature"],
                )
                for mod in self.modules:
                    mod.targetTemperature = self.controlSetup["ambientTemperature"]
                    mod.determineHeatingCooling(currentTemperature)
                    mod.rampStart = True
                self.soakTime = 0
                await self._operate(False, False, False)
            self.standby()

    def scheduleOperate(self, targetTemperature, soakTime=0):
        """Setup controller for a cooldown cycle and schedule cooldown coroutine."""
        for mod in self.modules:
            mod.targetTemperature = targetTemperature
            mod.determineHeatingCooling(self.controlSetup["ambientTemperature"])
            mod.rampStart = True
        self.soakTime = soakTime
        return self.eventLoop.create_task(
            coroExceptionHandler(self._operate(), self, True, self.multiprocessFlag)
        )

    async def _cycling(self, cycleSpecs):
        """Coroutine to run thermal cycling."""
        nCycles = cycleSpecs[0]
        cycleMethod = cycleSpecs[1]
        if cycleMethod not in ["insync", "staggered", "sequential"]:
            logging.warning(f"Method for thermal cycling ({cycleMethod}) not recognized. Options are: insync, staggered, or sequential. Please fix.")
            exit(0)
        cycleList = cycleSpecs[2:]
        if len(cycleList)>2:
            logging.warning("More than two cycle temperatures specified, this is not yet supported. Please fix.")
            exit(0)
        logging.info(
            "will do %i thermal cycle(s) from %s C to %s C with method %s",
            nCycles,
            cycleList[0][0],
            cycleList[1][0],
            cycleMethod
        )
        temp1 = cycleList[0][0]
        soak1 = cycleList[0][1]
        temp2 = cycleList[1][0]
        soak2 = cycleList[1][1]
        await self._setChiller(
            self.controlSetup["chillerOperatingTemperature"],
            self.controlSetup["chillerSettleTime"],
        )
        for i in range(nCycles):
            logging.info("starting cycle %i", i)

            if cycleMethod == "insync":
                for mod in self.modules:
                    mod.targetTemperature = temp1
                    mod.rampStart = True
                    self.soakTime = soak1
                    logging.info(f"Setting target temperature on {mod.name} to {mod.targetTemperature}")
                    mod.determineHeatingCooling(self.controlSetup["ambientTemperature"])

                await self._operate(False, False)
                for mod in self.modules:
                    mod.targetTemperature = temp2
                    mod.rampStart = True
                    self.soakTime = soak2
                    logging.info(f"Setting target temperature on {mod.name} to {mod.targetTemperature}")
                    mod.determineHeatingCooling(self.controlSetup["ambientTemperature"])
                await self._operate(False, False)

            elif cycleMethod == "staggered":
                if soak1 != soak2:
                    logging.warning("You requested to use the staggered cycling method with two different soak times - this is not supported. Please fix.")
                    exit(0)
                totalSetups = len(self.modules)
                self.soakTime = soak1 #Assuming this is the same as soak2
                for nmod in range(int(totalSetups/2)):
                    self.modules[nmod].targetTemperature = temp1
                    self.modules[nmod].rampStart = True
                    logging.info(f"Setting target temperature on {self.modules[nmod].name} to {self.modules[nmod].targetTemperature}")
                    mod.determineHeatingCooling(self.controlSetup["ambientTemperature"])
                for nmod in range(int(totalSetups/2), totalSetups):
                    self.modules[nmod].targetTemperature = temp2
                    self.modules[nmod].rampStart = True
                    logging.info(f"Setting target temperature on {self.modules[nmod].name} to {self.modules[nmod].targetTemperature}")
                    mod.determineHeatingCooling(self.controlSetup["ambientTemperature"])

                await self._operate(False, False)

                for nmod in range(int(totalSetups/2)):
                    self.modules[nmod].targetTemperature = temp2
                    self.modules[nmod].rampStart = True
                    logging.info(f"Setting target temperature on {self.modules[nmod].name} to {self.modules[nmod].targetTemperature}")
                    mod.determineHeatingCooling(self.controlSetup["ambientTemperature"])
                for nmod in range(int(totalSetups/2), totalSetups):
                    self.modules[nmod].targetTemperature = temp1
                    self.modules[nmod].rampStart = True
                    logging.info(f"Setting target temperature on {self.modules[nmod].name} to {self.modules[nmod].targetTemperature}")
                    mod.determineHeatingCooling(self.controlSetup["ambientTemperature"])

                await self._operate(False, False)

            elif cycleMethod == "sequential":
                for mod in self.modules:
                    for targetTemperature, soakTime in cycleList:
                        mod.targetTemperature = targetTemperature
                        mod.rampStart = True
                        self.soakTime = soakTime
                        logging.info(f"Setting target temperature on {mod.name} to {mod.targetTemperature}")
                        mod.determineHeatingCooling(self.controlSetup["ambientTemperature"])
                        await self._operate(False, False)
                    mod.targetTemperature = None


        logging.info(
            "thermal cycling completed - gracefully ramping to standby temperature of %4.1f C",
            self.controlSetup["ambientTemperature"],
        )
        for mod in self.modules:
            mod.targetTemperature = self.controlSetup["ambientTemperature"]
            mod.rampStart = True
            self.soakTime = 0
        await self._operate(False, False)
        self.standby()

    def scheduleCycling(self, cycleSpecs):
        """Setup thermal cycling.

        cycleSpecs specifies the cycling parameters in the form
        (nCycles, (minTemp,minSoaktime), (maxTemp,maxSoaktime), (method)).
        Available methods:
            1. "insync" - all setups cycled at the same temperature
            2. "staggered" - 1/2 setups cycled at min temp while the other setups go to max temp
            3. "sequential" - cycle one setup at a time
        """
        return self.eventLoop.create_task(
            coroExceptionHandler(self._cycling(cycleSpecs), self, True, self.multiprocessFlag)
        )

    def run(self, waitForTask=None):
        """Run controller event loop as previously configured.

        If waitForTask is not None, the event loop terminates when the given task completes.
        Otherwise the event loop doesn't stop until terminated via an exception or a signal.
        """
        if waitForTask is None:
            self.eventLoop.run_forever()
        else:
            self.eventLoop.run_until_complete(waitForTask)

    def standby(self):
        if self.running:
            logging.info("switching hardware to safe standby mode")
            for mod in self.modules:
                try:
                    mod.peltierPowerDevice.turnOff()
                    mod.peltierPowerDevice.setVoltageLevel(mod.peltierMinVoltage, True)
                    logging.info(f"turned off Peltier element power supply for {mod.name}")
                except Exception as e:
                    logging.error(f"Unable to turn off peltier element power supply for {mod.name}")
                    logging.error(e)
                try:
                    if mod.peltierHBridgeDevice is not None:
                        mod.peltierHBridgeDevice.setOff()
                        logging.info(f"Peltier relay set to cooling on {mod.name}")
                except Exception as e:
                    logging.error(f"Unable to control peltier relay for {mod.name}")
                    logging.error(e)
            if self.chiller is not None:
                chillerStandbyTemperature = self.controlSetup["chillerStandbyTemperature"]
                if chillerStandbyTemperature is not None:
                    try:
                        self.chiller.setTargetTemperature(chillerStandbyTemperature, True)
                        logging.info("chiller set to standby temperature")
                    except Exception as e:
                        logging.error("Unable to set chiller to standby temperature")
                        logging.error(e)
                else:
                    logging.info(
                        "no chiller standby temperature defined - not changing chiller setting"
                    )
            else:
                logging.info("no chiller configured - nothing to do")
            if self.controlError is None:
                self.controlHeartbeat.writeStatus(9)
            else:
                self.controlHeartbeat.writeStatus(self.controlError)
            self.running = False
        else:
            logging.info("controller in standby - leaving hardware as is")
        logging.info("End of standby")
        exit(0)
