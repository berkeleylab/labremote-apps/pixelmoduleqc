"""
Dew point sensor that implements relative humidity uncertainty for sensors such as HYT 271.
"""

import math

VAPORPRESSUREAPPROXIMATIONS = {
    "Sonntag90": {"a": 6.112, "b": 17.62, "c": 243.12},
    "Tetens30": {"a": 6.1078, "b": 17.27, "c": 237.3},
}


class DewPointSensor:
    def __init__(self, sensor, rhAccuracy, approximation="Sonntag90"):
        self.sensor = sensor
        self.rhUncertainty = rhAccuracy
        self.fit = VAPORPRESSUREAPPROXIMATIONS[approximation]

    def read(self):
        self.sensor.read()

    def temperature(self):
        return self.sensor.temperature()

    def humidity(self):
        return self.sensor.humidity()

    def dewPoint(self):
        return self.sensor.dewPoint()

    def dewPointLimit(self):
        """Calculate upper limit on dew point based on measured relative humidity and sensor accuracy."""
        # RH = self.humidity() + self.rhUncertainty
        RH = self.humidity()
        if RH <= 1.0:
            RH = 1
        T = self.temperature()
        gamma = (math.log(RH / 100.0) + (self.fit["b"] * T) / (self.fit["c"] + T)) / self.fit["b"]
        return self.fit["c"] * gamma / (1 - gamma)
