"""
Watchdog to ensure monitoring process keeps running and controller process doesn't get stuck.
"""

import datetime
import logging
import os
import smtplib
import socket
import subprocess
import time
from email.mime.text import MIMEText
from pathlib import Path
from signal import SIGINT, SIGKILL

from influxdb import InfluxDBClient

from pixmodqc.heartbeat import HeartBeat


def getConfDataByName(name, configurationLst):
    """Return element with given name from list of configuration data."""
    return next(filter(lambda x: x["name"] == name, configurationLst), None)


def loginInflux(credentials):
    """Creates client to login to influx db with given credentials"""
    return InfluxDBClient(
        credentials["host"],
        credentials["port"],
        credentials["username"],
        credentials["password"],
        credentials["database"],
    )


def sendQuery(client, query):
    """Sends query to client and returns the result"""
    return client.query(query)


def decodeTime(timeStr):
    timestamp_format = "%Y-%m-%dT%H:%M:%S.%fZ"
    dt_object = datetime.datetime.strptime(timeStr, timestamp_format)
    return (dt_object - datetime.datetime(1970, 1, 1)).total_seconds()


class Watchdog:
    def __init__(self, config):
        self.config = config
        self.wdHeartBeat = HeartBeat("watchdog", self.config.setupName)
        self.wdHeartBeat.acquireLock("watchdog cannot be run multiple times")
        self.wdHeartBeat.writeStatus(0)
        self.interval = self.config["watchdog"]["watchdogInterval"]
        self.maxMonitoringHeartBeatInterval = self.config["watchdog"][
            "maxMonitoringHeartBeatInterval"
        ]
        self.maxControlHeartBeatInterval = self.config["watchdog"]["maxControlHeartBeatInterval"]
        self.maxSWintHeartBeatInterval = self.config["watchdog"]["maxSWintHeartBeatInterval"]
        self.client = loginInflux(self.config["datasinks"][1])

    def alert(self, text, level="ALERT"):
        """Alert about problems."""
        fromAddr = "noreply@" + socket.gethostname()
        recipients = self.config["watchdog"]["alertEmailList"]
        subject = f"{level} from {self.config.setupName} watchdog: {text}"
        msgText = f"{level} from {self.config.setupName} watchdog"
        msgText += "\n%s\n" % (len(msgText) * "-")
        msgText += f"{time.ctime()}   {text}"
        msg = MIMEText(msgText)
        msg["From"] = fromAddr
        msg["To"] = recipients
        msg["Subject"] = subject
        smtp = smtplib.SMTP("localhost")
        smtp.sendmail(fromAddr, recipients.split(), msg.as_string())
        smtp.quit()

    def forceStandby(self):
        """Force hardware into standby mode (after terminating controller process)."""
        logging.warning("forcing hardware into standby mode")
        for m in self.config["control"]["moduleSetups"]:
            try:
                logging.debug(f"turning off Peltier power supply for {m.get('name')}")
                peltierPower = self.config.getDevice(m.get("peltierPower"))
                peltierPower.turnOff()
                try:
                    peltierPower.setVoltageLevel(m.get('peltierMinVoltage'), True)
                except Exception as e:
                    logging.error(f"exception trying to turn off Peltier power supply for {m.get('name')}: %s", str(e))
                    self.alert(
                        f"unable to force Peltier power supply off for {m.get('name')}: %s" % str(e),
                        level="CRITICAL ALERT",
                    )
            except Exception as e:
                logging.error(f"exception trying to turn off Peltier power supply for {m.get('name')}: %s", str(e))
                self.alert(
                    f"unable to force Peltier power supply off for {m.get('name')}: %s" % str(e),
                    level="CRITICAL ALERT",
                )

        chillerName = self.config["control"]["chiller"]
        if chillerName is not None:
            chillerStandbyTemperature = self.config["control"]["chillerStandbyTemperature"]
            if chillerStandbyTemperature is not None:
                try:
                    logging.debug("switching chiller to standby mode")
                    chiller = self.config.getDevice(chillerName)
                    chiller.setTargetTemperature(chillerStandbyTemperature, True)
                except Exception as e:
                    logging.error(
                        "exception trying to set chiller to standby temperature: %s",
                        str(e),
                    )
                    self.alert(
                        "unable to set chiller to standby temperature: %s" % str(e),
                        level="CRITICAL ALERT",
                    )

    def checkMonitoring(self):
        """Check if monitoring is running, restart if necessary, and log events."""
        self.monSetup = self.config["monitoring"]

        # Set log directory
        log_dir = Path("~/logs").expanduser()
        log_dir.mkdir(parents=True, exist_ok=True)  # Ensure the directory exists
        log_file_path = log_dir / "monitor_startup.log"  # New log file location

        # Loop through all devices
        for deviceGroup in self.monSetup["groups"]:
            if not deviceGroup["enable"]:
                continue
            monHeartBeat = HeartBeat(
                "monitoring", self.config.setupName + "_" + deviceGroup["name"]
            )
            (pid, lastBeatTime) = monHeartBeat.status()
            logging.debug(
                f"Heartbeat status of {self.config.setupName+'_'+deviceGroup['name']} is pid = {pid}, lastBeatTime = {time.time() - lastBeatTime}"
            )

            # Case 1: monitoring is not currently running, start it
            if pid is None:
                logging.error("monitoring not running - will start it")
                logging.debug(f"./scripts/coolman.py -c {self.config.configFile} monitor --debug")

                # Start monitoring process and log output
                with log_file_path.open("w") as log_file:
                    p = subprocess.Popen(
                        ["./scripts/coolman.py", "-c", self.config.configFile, "monitor", "--debug"],
                        stdout=log_file,
                        stderr=log_file,
                        start_new_session=True,
                    )

                logging.info("new monitoring process started, PID = %i", p.pid)
                #self.alert("monitoring was not running and had to be started") # Commented out for now

                time.sleep(3)  # Wait a few seconds for monitoring to get started

                # Re-check heartbeat
                (pid, lastBeatTime) = monHeartBeat.status()
                if pid is None:
                    logging.error("monitoring failed to start. Check /tmp/monitor_startup.log for details.")
                return

            # Case 2: monitoring lost heartbeat, kill it then restart it
            if time.time() - lastBeatTime > self.maxMonitoringHeartBeatInterval:
                logging.debug(f"Last beat time: {time.ctime(lastBeatTime)}")
                logging.error("monitoring running but lost heartbeat - will restart it")

                # Attempt to kill the process if running
                try:
                    os.kill(pid, 0) # Let's not actually kill the signal for now
                    #os.kill(pid, SIGKILL)
                    logging.info(f"Terminated existing monitoring process (PID={pid})")
                except ProcessLookupError:
                    logging.warning(f"Process {pid} not found; assuming it has already stopped")

                # Re-start monitoring process and log output
                with log_file_path.open("w") as log_file:
                    p = subprocess.Popen(
                        ["./scripts/coolman.py", "-c", self.config.configFile, "monitor", "--debug"],
                        stdout=log_file,
                        stderr=log_file,
                        start_new_session=True,
                    )

                logging.info("new monitoring process started, PID = %i", p.pid)
                self.alert(
                    'monitoring lost {self.config.setupName+"_"+deviceGroup["name"]} heartbeat and had to be restarted'
                )

                time.sleep(3)  # Wait a few seconds for monitoring to get started

                # Re-check heartbeat
                (pid, lastBeatTime) = monHeartBeat.status()
                if pid is None:
                    logging.error("monitoring failed to start. Check /tmp/monitor_startup.log for details.")
                return

            # Case 3: monitoring is running normally
            logging.debug(
                "monitoring process %i, last heart beat %2.1f s ago",
                pid,
                time.time() - lastBeatTime,
            )

    def intController(self):
        controlHeartBeat = HeartBeat("control", self.config.setupName)
        (pid, lastBeatTime) = controlHeartBeat.status()
        if pid is not None:
            os.kill(pid, SIGINT)

    def checkHWinterlock(self):
        HWintname = "HWinterlock"
        getConfDataByName(HWintname, self.config.config["monitorsensors"])
        measName = self.config.config["monitoring"]["measurementName"]
        query = f'SELECT "V" FROM "{measName}" WHERE ("Setup" = \'{self.config.setupName}\') AND ("Quantity" = \'{HWintname}\') ORDER BY time DESC LIMIT 1'
        result = sendQuery(self.client, query)
        V = [float(record.get("V")) for record in result.get_points()]
        T = [record.get("time") for record in result.get_points()]
        if len(V) == 0:
            logging.error("No data for HW interlock found on influxDB, aborting")
            self.intController()
        elif V[0] < 0.001:
            logging.error("HW interlock triggered. Aborting")
            self.intController()
        elif time.time() - decodeTime(T[0]) > 10:
            logging.error("No data for HW interlock found in last 10 seconds. Aborting")
            self.intController()

    def checkController(self):
        controlHeartBeat = HeartBeat("control", self.config.setupName)
        (pid, lastBeatTime) = controlHeartBeat.status()
        lastBeat = time.ctime(lastBeatTime) if lastBeatTime else "---"
        if pid is None:
            logging.debug("controller not running, last ran at %s", lastBeat)
            return
        if time.time() - lastBeatTime > self.maxControlHeartBeatInterval:
            logging.error("controller process %i unresponive - aborting", pid)
            os.kill(pid, SIGKILL)
            time.sleep(2)  # Wait a few seconds for controller process to die
            self.forceStandby()
            controlHeartBeat.writeStatus(13, checkLock=False)
            self.alert("controller unresponsive (PID %i) - terminated" % pid)
        else:
            logging.debug(
                "controller process %i, last heart beat %2.1f s ago",
                pid,
                time.time() - lastBeatTime,
            )

    def checkSWinterlock(self):
        swintConfig = "/home/fhartanto/software-interlock/sourcebox-cfg_TEST.json" # Hard-coded for now, the interlock config file
        swintHeartBeat = HeartBeat("software-interlock", self.config.setupName)
        (pid, lastBeatTime) = swintHeartBeat.status()
        lastBeat = time.ctime(lastBeatTime) if lastBeatTime else "---"

        # Set log directory
        log_dir = Path("~/logs").expanduser()
        log_dir.mkdir(parents=True, exist_ok=True)  # Ensure the directory exists
        log_file_path = log_dir / "interlock_startup.log"  # New log file location

        # Case 1: SWint is not already running, start it
        if pid is None:
            logging.error(f"SW interlock not running, last ran at {lastBeat}, will start it")
            logging.debug(f"Running: /home/fhartanto/software-interlock/interlock.py -c {swintConfig}")

            # Start SW interlock process and capture output
            with log_file_path.open("w") as log_file:
                p = subprocess.Popen(
                    ["python", "/home/fhartanto/software-interlock/interlock.py", "-c", swintConfig],
                    stdout=log_file,
                    stderr=log_file,
                    start_new_session=True,
                )

            logging.info(f"New SW interlock process started, PID = {p.pid}")
            self.alert("SW interlock was not running and had to be restarted")

            time.sleep(3) # Wait a few seconds for SW int to start

            # Re-check heartbeat
            (pid, lastBeatTime) = swintHeartBeat.status()
            if pid is None:
                logging.error("SW interlock failed to start. Check /tmp/interlock_startup.log for details.")
            return

        # Case 2: SWint not responsive, kill and then start it
        if time.time() - lastBeatTime > self.maxSWintHeartBeatInterval:
            logging.error(f"SW interlock process {pid if pid else 'UNKNOWN'} unresponsive - aborting")

            # Attempt to kill the process if running
            try:
                os.kill(pid, SIGKILL)
                logging.info(f"Terminated existing SW interlock process (PID={pid})")
            except ProcessLookupError:
                logging.warning(f"Process {pid} not found; assuming it has already stopped")

            time.sleep(2)
            self.forceStandby()
            swintHeartBeat.writeStatus(0, checkLock=False)
            self.alert(f"SW interlock unresponsive (PID {pid if pid else 'UNKNOWN'}) - terminated")

            logging.error("Attempting to restart SW interlock")
            logging.debug(f"Running: /home/fhartanto/software-interlock/interlock.py -c {swintConfig}")

            with log_file_path.open("w") as log_file:
                p = subprocess.Popen(
                    ["python", "/home/fhartanto/software-interlock/interlock.py", "-c", swintConfig],
                    stdout=log_file,
                    stderr=log_file,
                    start_new_session=True,
                )

            logging.info(f"New SW interlock process started, PID = {p.pid}")
            self.alert("SW interlock was not running and had to be restarted")

            time.sleep(3) # Wait a few seconds for SW int to start

            # Re-check heartbeat
            (pid, lastBeatTime) = swintHeartBeat.status()
            if pid is None:
                logging.error("SW interlock restart failed. Check /tmp/interlock_startup.log for details.")

        # Case 3: SWint is running normally
        else:
            logging.debug(
                "SW interlock process %i, last heart beat %2.1f s ago",
                pid,
                time.time() - lastBeatTime,
            )

    def run(self):
        """Run the watchdog perpetually."""
        while True:
            logging.debug("watchdog waking up, checking ...")
            self.checkHWinterlock()
            self.checkMonitoring()
            self.checkController()
            self.checkSWinterlock()
            self.wdHeartBeat.beat()
            time.sleep(self.interval)
