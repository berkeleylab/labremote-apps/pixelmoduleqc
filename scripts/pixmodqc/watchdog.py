"""
Watchdog to ensure monitoring process keeps running and controller process doesn't get stuck.
"""

import datetime
import logging
import os
import smtplib
import socket
import subprocess
import time
from email.mime.text import MIMEText
from signal import SIGINT, SIGKILL

from influxdb import InfluxDBClient

from pixmodqc.heartbeat import HeartBeat


def getConfDataByName(name, configurationLst):
    """Return element with given name from list of configuration data."""
    return next(filter(lambda x: x["name"] == name, configurationLst), None)


def loginInflux(credentials):
    """Creates client to login to influx db with given credentials"""
    return InfluxDBClient(
        credentials["host"],
        credentials["port"],
        credentials["username"],
        credentials["password"],
        credentials["database"],
    )


def sendQuery(client, query):
    """Sends query to client and returns the result"""
    return client.query(query)


def decodeTime(timeStr):
    timestamp_format = "%Y-%m-%dT%H:%M:%S.%fZ"
    dt_object = datetime.datetime.strptime(timeStr, timestamp_format)
    return (dt_object - datetime.datetime(1970, 1, 1)).total_seconds()


class Watchdog:
    def __init__(self, config):
        self.config = config
        self.wdHeartBeat = HeartBeat("watchdog", self.config.setupName)
        self.wdHeartBeat.acquireLock("watchdog cannot be run multiple times")
        self.wdHeartBeat.writeStatus(0)
        self.interval = self.config["watchdog"]["watchdogInterval"]
        self.maxMonitoringHeartBeatInterval = self.config["watchdog"][
            "maxMonitoringHeartBeatInterval"
        ]
        self.maxControlHeartBeatInterval = self.config["watchdog"]["maxControlHeartBeatInterval"]
        self.client = loginInflux(self.config["datasinks"][1])

    def alert(self, text, level="ALERT"):
        """Alert about problems."""
        fromAddr = "noreply@" + socket.gethostname()
        recipients = self.config["watchdog"]["alertEmailList"]
        subject = f"{level} from {self.config.setupName} watchdog: {text}"
        msgText = f"{level} from {self.config.setupName} watchdog"
        msgText += "\n%s\n" % (len(msgText) * "-")
        msgText += f"{time.ctime()}   {text}"
        msg = MIMEText(msgText)
        msg["From"] = fromAddr
        msg["To"] = recipients
        msg["Subject"] = subject
        smtp = smtplib.SMTP("localhost")
        smtp.sendmail(fromAddr, recipients.split(), msg.as_string())
        smtp.quit()

    def forceStandby(self):
        """Force hardware into standby mode (after terminating controller process)."""
        logging.warning("forcing hardware into standby mode")
        try:
            logging.debug("turning off Peltier power supply")
            peltierPower = self.config.getDevice(self.config["control"]["peltierPower"])
            peltierPower.turnOff()
            peltierPower.setVoltageLevel(self.config["control"]["peltierMinVoltage"])
        except Exception as e:
            logging.error("exception trying to turn off Peltier power supply: %s", str(e))
            self.alert(
                "unable to force Peltier power supply off: %s" % str(e),
                level="CRITICAL ALERT",
            )

        chillerName = self.config["control"]["chiller"]
        if chillerName is not None:
            chillerStandbyTemperature = self.config["control"]["chillerStandbyTemperature"]
            if chillerStandbyTemperature is not None:
                try:
                    logging.debug("switching chiller to standby mode")
                    chiller = self.config.getDevice(chillerName)
                    chiller.setTargetTemperature(chillerStandbyTemperature)
                except Exception as e:
                    logging.error(
                        "exception trying to set chiller to standby temperature: %s",
                        str(e),
                    )
                    self.alert(
                        "unable to set chiller to standby temperature: %s" % str(e),
                        level="CRITICAL ALERT",
                    )

    def checkMonitoring(self):
        self.monSetup = self.config["monitoring"]
        for deviceGroup in self.monSetup["groups"]:
            monHeartBeat = HeartBeat(
                "monitoring", self.config.setupName + "_" + deviceGroup["name"]
            )
            (pid, lastBeatTime) = monHeartBeat.status()
            logging.debug(
                f"Heartbeat status of {self.config.setupName+'_'+deviceGroup['name']} is pid = ",
                pid,
                ", lastBeatTime = ",
                time.time() - lastBeatTime,
            )
            if pid is None:
                logging.error("monitoring not running - will start it")
                logging.debug(f"./scripts/coolman.py -c {self.config.configFile} -s monitor")
                p = subprocess.Popen(
                    ["./scripts/coolman.py", "-c", self.config.configFile, "monitor"],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                    start_new_session=True,
                )
                logging.info("new monitoring process started, PID = %i", p.pid)
                self.alert("monitoring was not running and had to be restarted")
                time.sleep(3)  # Wait a few seconds for monitoring to get started
            elif time.time() - lastBeatTime > self.maxMonitoringHeartBeatInterval:
                logging.error("monitoring running but lost heartbeat - will restart it")
                os.kill(pid)
                p = subprocess.Popen(
                    ["./scripts/coolman.py", "-c", self.config.configFile, "monitor"],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                    start_new_session=True,
                )
                logging.info("new monitoring process started, PID = %i", p.pid)
                self.alert(
                    'monitoring lost {self.config.setupName+"_"+deviceGroup["name"]} heartbeat and had to be restarted'
                )
                time.sleep(3)  # Wait a few seconds for monitoring to get started
            else:
                logging.debug(
                    "monitoring process %i, last heart beat %2.1f s ago",
                    pid,
                    time.time() - lastBeatTime,
                )

    def intController(self):
        controlHeartBeat = HeartBeat("control", self.config.setupName)
        (pid, lastBeatTime) = controlHeartBeat.status()
        if pid is not None:
            os.kill(pid, SIGINT)

    def checkHWinterlock(self):
        HWintname = "HWinterlock"
        getConfDataByName(HWintname, self.config.config["monitorsensors"])
        measName = self.config.config["monitoring"]["measurementName"]
        query = f'SELECT "V" FROM "{measName}" WHERE ("Setup" = \'{self.config.setupName}\') AND ("Quantity" = \'{HWintname}\') ORDER BY time DESC LIMIT 1'
        result = sendQuery(self.client, query)
        V = [float(record.get("V")) for record in result.get_points()]
        T = [record.get("time") for record in result.get_points()]
        if len(V) == 0:
            logging.error("No data for HW interlock found on influxDB, aborting")
            self.intController()
        elif V[0] < 0.001:
            logging.error("HW interlock triggered. Aborting")
            self.intController()
        elif time.time() - decodeTime(T[0]) > 10:
            logging.error("No data for HW interlock found in last 10 seconds. Aborting")
            self.intController()

    def checkController(self):
        controlHeartBeat = HeartBeat("control", self.config.setupName)
        (pid, lastBeatTime) = controlHeartBeat.status()
        lastBeat = time.ctime(lastBeatTime) if lastBeatTime else "---"
        if pid is None:
            logging.debug("controller not running, last ran at %s", lastBeat)
            return
        if time.time() - lastBeatTime > self.maxControlHeartBeatInterval:
            logging.error("controller process %i unresponive - aborting", pid)
            os.kill(pid, SIGKILL)
            time.sleep(2)  # Wait a few seconds for controller process to die
            self.forceStandby()
            controlHeartBeat.writeStatus(13, checkLock=False)
            self.alert("controller unresponsive (PID %i) - terminated" % pid)
        else:
            logging.debug(
                "controller process %i, last heart beat %2.1f s ago",
                pid,
                time.time() - lastBeatTime,
            )

    def run(self):
        """Run the watchdog perpetually."""
        while True:
            logging.debug("watchdog waking up, checking ...")
            self.checkHWinterlock()
            self.checkMonitoring()
            self.checkController()
            self.wdHeartBeat.beat()
            time.sleep(self.interval)
