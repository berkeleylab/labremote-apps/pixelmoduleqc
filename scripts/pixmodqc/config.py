"""
Read and process configuration files for pixel cooling unit and QC.
"""
__author__ = "Juerg Beringer"

import datetime
import json
import logging
import time
from pathlib import Path

import labRemote.com
import labRemote.devcom
import labRemote.ec

from pixmodqc.adcsensor import *
from pixmodqc.analogsensor import *
from pixmodqc.dewpointsensor import DewPointSensor
from pixmodqc.digitalsensor import DigitalSensor
from pixmodqc.ntcmeter import NTCMeter
from pixmodqc.relay import Relay

# Maps strings in config file to CharSize
CharSize = {
    "CS5": "CharSize5",
    "CS6": "CharSize6",
    "CS7": "CharSize7",
    "CS8": "CharSize8",
}


def getConfDataByName(name, configurationLst):
    """Return element with given name from list of configuration data."""
    return next(filter(lambda x: x["name"] == name, configurationLst), None)


class Config:
    def __init__(self, configFile):
        self.configFile = configFile
        with Path(configFile).open() as c:
            self.config = json.load(c)
        self.setupName = self.config["setupname"]
        logging.debug(f"Running setup = {self.setupName}")
        self.coms = {}
        self.devices = {}
        self.dataSinkConf = labRemote.ec.DataSinkConf(self.config)
        self.equipConf = labRemote.ec.EquipConf(self.config)
        # reference to Controller, if any, so it can be monitored like a device
        self.controller = None

    def __getitem__(self, item):
        return self.config[item]

    def summary(self):
        """Print summary of config file."""
        logging.info("Summary of config file %s:\n" % self.configFile)
        for k, v in sorted(self.config.items()):
            logging.info(k)
            try:
                if isinstance(v, list):
                    logging.info(", ".join([x["name"] for x in v]))
                else:
                    for i in v:
                        logging.info("    %s" % i)
                        if isinstance(v[i], list):
                            logging.info(", ".join([x["name"] for x in v[i]]))
            except Exception:
                pass

    def dump(self):
        """Nicely print contents of config file."""
        logging.info("Contents of config file %s:\n" % self.configFile)
        import pprint

        logging.info(pprint.pprint(self.config))

    def getCom(
        self,
        port,
        baud_rate=9600,
        termination="\r\n",
        terminationReturn=None,
        parityBit=False,
        twoStopBits=False,
        flowControl=False,
        charsize="CharSize8",
        timeout=5,
    ):
        """Return configured serial communication interface."""
        if port in self.coms:
            return self.coms[port]
        com = labRemote.com.TextSerialCom(
            port,
            getattr(labRemote.com.SerialCom.BaudRate, baud_rate.replace("B", "Baud")),
            parityBit,
            twoStopBits,
            flowControl,
            getattr(labRemote.com.SerialCom.CharSize, charsize),
        )
        if terminationReturn is not None:
            com.setTermination(termination, terminationReturn)
        else:
            com.setTermination(termination, termination)
        if timeout is not None:
            com.setConfiguration({"timeout": timeout})
        com.init()
        self.coms[port] = com
        return com

    def getDevice(self, deviceName):
        """Get device as specified in setup section of config file."""
        if deviceName in self.devices:
            return self.devices[deviceName]
        device_info = getConfDataByName(deviceName, self.config["monitorsensors"])
        if device_info is None:
            raise ValueError("no configuration found for device %s" % deviceName)
        if device_info["type"] == "CONTROLLER":
            device = self.controller
        elif device_info["type"] == "DEVCOMUINO":
            device = self.getCom(device_info["port"], device_info["baudrate"])
            time.sleep(
                2
            )  # ensure Arduino bootloader triggered by each connect to USB port has timed out (see datasheet)
        elif device_info["type"] == "RELAY":
            arduino = self.getDevice(device_info["arduino"])
            device = Relay(arduino, device_info["arduinoDigitalPin"])
        elif device_info["type"] == "DIGITALSENSOR":
            arduino = self.getDevice(device_info["arduino"])
            device = DigitalSensor(arduino, device_info["arduinoDigitalPin"], deviceName)
        elif device_info["type"] == "ANALOGSENSOR":
            arduino = self.getDevice(device_info["arduino"])
            cls = globals()[device_info["sensor"]]
            device = cls(
                arduino,
                device_info["arduinoPin"],
                deviceName,
                device_info.get("supplyVoltage", 5.0),
            )
        elif device_info["type"] == "DEWPOINTSENSOR":
            sensor = self.getDevice(device_info["sensor"])
            device = DewPointSensor(sensor, device_info["accuracy"])
        elif device_info["type"] == "NTC":
            arduino = self.getDevice(device_info["arduino"])
            adcDevice = labRemote.devcom.ADCDevComuino(device_info["V"], arduino)
            device = labRemote.devcom.NTCSensor(
                device_info["arduinoPin"],
                adcDevice,
                False,  # do not use Steinhart parametrization
                device_info["refTemp"],
                device_info["Rref"],
                device_info["Bntc"],
                False,
                device_info["Rdiv"],
                device_info["V"],
            )
        elif device_info["type"] == "NTCMeter":
            meter = self.equipConf.getMeter(device_info["meter"])
            device = NTCMeter(meter, device_info["meterChannel"])
        elif device_info["type"] == "SHT85":
            arduino = self.getDevice(device_info["arduino"])
            i2c = labRemote.devcom.I2CDevComuino(int(device_info["address"], 16), arduino)
            device = labRemote.devcom.SHT85(i2c)
        elif device_info["type"] == "HYT271":
            arduino = self.getDevice(device_info["arduino"])
            i2c = labRemote.devcom.I2CDevComuino(int(device_info["address"], 16), arduino)
            device = labRemote.devcom.HYT271(i2c)
        elif device_info["type"] == "PolySciLM":
            com = self.getCom(
                device_info["port"], device_info["baud"], termination="\r", timeout=None
            )
            device = labRemote.chiller.PolySciLM(device_info["name"])
            device.setCom(com)
            device.init()
        elif device_info["type"] == "Julabo":
            com_info = getConfDataByName(device_info["device"], self.config["devices"])[
                "communication"
            ]
            com = self.getCom(
                com_info["port"],
                com_info["baudrate"],
                termination=com_info["termination"],
                terminationReturn=com_info["returnTermination"],
                charsize=CharSize.get(com_info["charsize"]),
                timeout=None,
            )
            device = labRemote.chiller.Julabo(device_info["name"])
            device.setCom(com)
            device.init()
        elif device_info["type"] == "PSChannel":
            device = self.equipConf.getPowerSupplyChannel(deviceName)
        elif device_info["type"] == "METERChannel":
            device = self.equipConf.getMeter(device_info["device"])
        elif device_info["type"] == "FT232":
            mpsse = labRemote.devcom.MPSSEChip
            protocol = mpsse.Protocol.I2C
            speed = mpsse.Speed.FOUR_HUNDRED_KHZ
            endianness = mpsse.Endianness.MSBFirst
            # If only one FT232H is connected to the computer the serial number (last field) may be left empty. It is needed to distinguish between multiple FT232H chips.
            ft232 = labRemote.devcom.FT232H(protocol, speed, endianness, "", "")
            device = labRemote.devcom.I2CFTDICom(ft232, int(device_info["mux0_address"], 16))
        elif device_info["type"] == "HYT271_I2C":
            com_mux0 = self.getDevice(device_info["ft232"])
            channel = labRemote.devcom.PCA9548ACom(
                int(device_info["device_address"], 16),
                device_info["channelnum"],
                com_mux0,
            )
            device = labRemote.devcom.HYT271(channel)
        elif device_info["type"] == "ADCSensor":
            com_mux0 = self.getDevice(device_info["ft232"])
            i2c_channel = labRemote.devcom.PCA9548ACom(
                int(device_info["device_address"], 16),
                device_info["channelnum"],
                com_mux0,
            )
            adc = labRemote.devcom.ADS1015(i2c_channel)
            adc.setGain(adc.Gain(device_info["gain"]))
            cls = globals()[device_info["sensor"]]
            device = cls(adc, device_info["adc_channel"])
        else:
            raise ValueError(
                "undefined device type %s found in configuration for %s"
                % (device_info["type"], deviceName)
            )
        self.devices[deviceName] = device
        return device

    def getDeviceList(self):
        """Get list of names of all devices listed in setup."""
        return [x["name"] for x in self[self.setupName]["devices"]]

    def makeStream(self, streamName):
        """Make a new data stream."""
        return self.dataSinkConf.getDataStream(streamName)


class DeviceStream:
    def __init__(self, deviceName, streamName, measurementName, config, autoConfigure=True):
        self.deviceName = deviceName
        self.streamName = streamName
        self.measurementName = measurementName
        self.setupName = config.setupName
        self.configData = getConfDataByName(deviceName, config["monitorsensors"])
        self.device = config.getDevice(deviceName)
        self.stream = config.makeStream(streamName)
        self.prescale = 1
        if autoConfigure:
            self.configureStream()

    def configureStream(self):
        self.stream.setTag("Setup", self.setupName)
        self.stream.setTag("Quantity", self.configData["quantity"])
        # Set any additional tags if specified in the setup
        if "tags" in self.configData:
            for k, v in self.configData["tags"].items():
                self.stream.setTag(k, v)
        if self.configData.get("prescale"):
            self.prescale = self.configData.get("prescale")

    def measure(self, timeStamp=None):
        if timeStamp is None:
            timeStamp = datetime.datetime.now()
        data = {}
        logging.debug(
            f"Attempting to read {self.deviceName} with read(), which is of type {type(self.device)}"
        )
        try:
            self.device.read()
        except AttributeError:
            pass
        except Exception as e:
            logging.error("failed to read sensor data: %s", str(e))
            # FIXME: should this raise an Exception rather than continue?
            return data
        self.stream.startMeasurement(self.measurementName, timeStamp)
        dataStrings = []
        for fieldName, methodName in self.configData["fields"].items():
            try:
                # Must supply channel if reading from multimeter
                if methodName == "measureDCV":
                    if type(self.configData["channel"]) != int:
                        logging.error(
                            "Must supply channel (int) to read from multimeter: %s"
                            % (self.deviceName)
                        )
                        value = None
                    else:
                        value = getattr(self.device, methodName)(self.configData["channel"])
                else:
                    value = getattr(self.device, methodName)()
            except AttributeError as e:
                logging.error(f"{self.deviceName} has no method {methodName} - check your configuration")
                raise NotImplementedError(e) from None
            except Exception as e:
                logging.error("exception reading %s from %s", methodName, self.deviceName)
                raise RuntimeError() from e
            if value is not None:
                self.stream.setField(fieldName, value)
                data[fieldName] = value
                dataStrings.append(f"{fieldName} = {value:.4g}")
        if len(dataStrings) > 0:
            self.stream.recordPoint()
        self.stream.endMeasurement()
        logging.debug(
            "measure: %-20s %s",
            self.deviceName,
            ", ".join(dataStrings) or "NO VALID DATA",
        )
        return data
