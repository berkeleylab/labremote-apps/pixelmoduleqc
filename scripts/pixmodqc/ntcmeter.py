"""
NTC resistance measured through meter, no ADC
"""
import math


def RtoC(Rntc, refTemp, Rref, Bntc):
    if (Rntc >= 3.e+38):
        return -273.15
    absT = 1. / ((1/refTemp) + (1/Bntc) * math.log(Rntc/Rref))
    return absT - 273.15

class NTCMeter:
    def __init__(
        self,
        meter,
        meterChannel,
        refTemp=298.15,
        Rref=10000,
        Bntc=3435,
    ):
        self.meter = meter
        self.meterChannel = meterChannel
        self.refTemp = refTemp
        self.Rref = Rref
        self.Bntc = Bntc

    def temperature(self):
        Rntc = self.meter.measureRES(self.meterChannel, False)
        return RtoC(Rntc, self.refTemp, self.Rref, self.Bntc)
