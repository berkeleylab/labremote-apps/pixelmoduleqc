"""
Heartbeat and locking tools to ensure processes are running.
"""

import json
import logging
import os
import stat
import time
from pathlib import Path

from filelock import FileLock, Timeout

logging.getLogger("filelock").setLevel(logging.INFO)


class HeartBeatLocked(Exception):
    pass


class HeartBeat:
    def __init__(self, functionName, setup):
        self.function = functionName
        self.setup = setup
        self.name = f"{setup}-{functionName}"
        self.basepath = "/tmp/coolman-%s" % self.name
        self.lockfile = self.basepath + ".lock"
        self.pidfile = self.basepath + ".pid"
        self.dataFile = self.basepath + ".json"
        self.lock = FileLock(self.lockfile, timeout=0)

    def acquireLock(self, errorMsg=None):
        """Acquire lock for this heartbeat. Raises Timeout exception if lock exists already."""
        try:
            logging.debug("acquiring heartbeat lock for %s", self.name)
            self.lock.acquire()
            with Path(self.pidfile).open(mode='w') as pidfile:
                pidfile.write("%i\n" % os.getpid())
                """Give permissions to others to access the lock and PID files"""
                try:
                    Path(self.lockfile).chmod(stat.S_IRUSR | stat.S_IWUSR |stat.S_IRGRP | stat.S_IWGRP | stat.S_IROTH | stat.S_IWOTH)
                except Exception:
                    logging.debug("Couldn't change access to %s already made.", self.lockfile)
                try:
                    Path(self.pidfile).chmod(stat.S_IRUSR | stat.S_IWUSR |stat.S_IRGRP | stat.S_IWGRP | stat.S_IROTH | stat.S_IWOTH)
                except Exception:
                    logging.debug("Couldn't change access to %s, already made.", self.pidfile)
        except Timeout:
            if errorMsg:
                logging.error(errorMsg)
            raise HeartBeatLocked(
                "%s already locked by process with PID = %i" % (self.name, self.readPid())
            ) from None

    def beat(self):
        """Issue heartbeat."""
        if self.lock.is_locked:
            os.utime(self.pidfile)
        else:
            raise RuntimeError("must acquire lock for %s before issuing heartbeat" % self.name)

    def readPid(self):
        """Return PID of last process holding the lock, or None."""
        try:
            return int(Path(self.pidfile).open().read().strip())
        except Exception:
            return None

    def status(self):
        """Return heartbeat status as (PID, time of last heartbeat)."""
        pid = self.readPid()
        try:
            os.kill(pid, 0)
        except Exception:
            pid = None
        try:
            lastBeat = os.path.getmtime(self.pidfile)
        except Exception:
            lastBeat = 0
        return (pid, lastBeat)

    def isRunning(self, maxHeartBeatInterval=30):
        """Return True if heart beat process is running and last beat was within maxHeartBeatInterval."""
        (pid, lastBeatTime) = self.status()
        return pid is not None and time.time() - lastBeatTime <= maxHeartBeatInterval

    def writeData(self, data, checkLock=True):
        """Write heartbeat data to data file."""
        if checkLock and not self.lock.is_locked:
                raise RuntimeError("must acquire lock for %s before writing data" % self.name)
        with Path(self.dataFile).open(mode='w') as dataFile:
            json.dump(data, dataFile)

    def writeStatus(self, status, checkLock=True):
        """Write status data only to data file."""
        self.writeData({"status": status}, checkLock)

    def readData(self):
        """Read heartbeat data."""
        try:
            with Path(self.dataFile).open() as dataFile:
                return json.load(dataFile)
        except Exception:
            return None
