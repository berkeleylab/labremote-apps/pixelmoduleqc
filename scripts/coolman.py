#!/usr/bin/env python3
"""
Command line script to control pixel cooling unit. See `coolman.py -h` for usage.
"""
__author__ = "Juerg Beringer"

import argparse
import logging
import os
import sys
import time
import traceback

import labRemote
from pixmodqc.config import Config
from pixmodqc.controller import CoolingUnitController
from pixmodqc.heartbeat import HeartBeat
from pixmodqc.watchdog import Watchdog

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Command line script for controlling the pixel cooling unit"
    )
    parser.add_argument(
        "-v", "--verbose", action="store_true", default=False, help="verbose output"
    )
    parser.add_argument(
        "--vpid",
        action="store_true",
        default=False,
        help="enable verbose output from PID loop",
    )
    parser.add_argument("--debug", action="store_true", default=False, help="debug output")
    parser.add_argument(
        "-i",
        "--interactive",
        action="store_true",
        default=False,
        help="interactive mode",
    )
    parser.add_argument(
        "-c", "--config", default="coolman.json", help="location of configuration file"
    )
    parser.add_argument(
        "-o",
        "--outputstream",
        default="monitoring",
        help="name of stream to write monitoring data to (default: monitoring)",
    )
    parser.add_argument(
        "-m",
        "--multiprocess",
        action="store_true",
        default=False,
        help="run in multiprocess mode",
    )
    parser.add_argument(
        "cmd",
        help="desired action",
        choices=[
            "monitor",
            "operate",
            "cycling",
            "chiller",
            "watchdog",
            "reset",
            "status",
            "showconfig",
            "arduino",
            "debug",
        ],
    )
    parser.add_argument("args", nargs="*", help="arguments for desired action")
    options = parser.parse_args()

    logging.basicConfig(
        format="%(asctime)s %(levelname)-8s %(message)s", datefmt="%a %b %d %X %Z %Y"
    )
    logger = logging.getLogger()
    labRemote.setLogLevel(labRemote.loglevel_e.logERROR)
    if options.verbose:
        logger.setLevel(logging.INFO)
        labRemote.setLogLevel(labRemote.loglevel_e.logINFO)
    if options.debug:
        logger.setLevel(logging.DEBUG)
        labRemote.setLogLevel(labRemote.loglevel_e.logDEBUG)
    if options.interactive:
        os.environ["PYTHONINSPECT"] = "1"

    # Overall exception handling
    try:
        # Read hardware configuration file
        config = Config(options.config)

        useasyncio = True

        # Monitoring
        if options.cmd == "monitor":
            useasyncio = False
            controller = CoolingUnitController(
                config, options.vpid, options.multiprocess, useasyncio
            )
            controller.scheduleMonitoring(options.outputstream)
            controller.run()

        # Cool down or heat up to given temperature and soak there for specified time
        if options.cmd == "operate":
            try:
                targetTemperature = float(options.args[0])
            except Exception:
                sys.exit("ERROR: must provide desired temperature")
            try:
                soakTime = float(options.args[1])
            except Exception:
                soakTime = -1
            controller = CoolingUnitController(
                config, options.vpid, options.multiprocess, useasyncio
            )
            if options.multiprocess:
                controller.prepareForControl()
                controller.run(controller.scheduleOperate(targetTemperature, soakTime))
            else:
                controller.prepareForControl()
                controller.scheduleOperate(targetTemperature, soakTime)
                controller.run()

        # Run multistep thermal cycling as configured in config file
        if options.cmd == "cycling":
            try:
                cycleSpecs = config["cycles"][options.args[0]]
            except Exception:
                sys.exit("ERROR: no or non-existent thermal cycle definition specified")
            controller = CoolingUnitController(
                config, options.vpid, options.multiprocess, useasyncio
            )
            if options.multiprocess:
                controller.prepareForControl()
                controller.run(controller.scheduleCycling(cycleSpecs))
            else:
                controller.prepareForControl()
                controller.scheduleCycling(cycleSpecs)
                controller.run()

        if options.cmd == "watchdog":
            watchdog = Watchdog(config)
            watchdog.run()

        # Reset controller error condition
        if options.cmd == "reset":
            controlHeartBeat = HeartBeat("control", config.setupName)
            controlHeartBeat.acquireLock("controller is still running - please abort first")
            controlHeartBeat.writeStatus(0)

        # Basic chiller control
        if options.cmd == "chiller":
            try:
                cmd = options.args[0].lower()
                if cmd not in ("on", "off"):
                    temp = float(cmd)
            except Exception:
                sys.exit("ERROR: chiller argument must be temperature, 'on' or 'off'")
            chillerName = config["control"]["chiller"]
            if chillerName is not None:
                try:
                    chiller = config.getDevice(chillerName)
                    if cmd == "on":
                        chiller.turnOn(True)
                    elif cmd == "off":
                        chiller.turnOff()
                    else:
                        chiller.setTargetTemperature(float(cmd), True)
                    time.sleep(5)
                    if chiller.getStatus():
                        logging.info(
                            "chiller now at %4.1f C (target %4.1f C)"
                            % (
                                chiller.measureTemperature(),
                                chiller.getTargetTemperature(),
                            )
                        )
                    else:
                        logging.info("chiller is now off")
                except Exception as e:
                    logging.info("  %-25s  %s" % ("Chiller", "powered off or not responding"))
                    logging.info(e)
            else:
                sys.exit("ERROR: no chiller configured")

        # Show system status
        if options.cmd == "status":
            # coolman processes
            logging.info("Processes:")
            wdPid, lastBeat = HeartBeat("watchdog", options.setup).status()
            lastBeat = time.ctime(lastBeat) if lastBeat else "---"
            if wdPid is None:
                logging.info("  %-30s  last ran at %s" % ("Watchdog NOT RUNNING", lastBeat))
            else:
                logging.info("  %-30s  last heartbeat at %s" % ("Watchdog PID %i" % wdPid, lastBeat))
            monPid, lastBeat = HeartBeat("monitoring", options.setup).status()
            lastBeat = time.ctime(lastBeat) if lastBeat else "---"
            if monPid is None:
                logging.info("  %-30s  last ran at %s" % ("Monitoring NOT RUNNING", lastBeat))
            else:
                logging.info("  %-30s  last heartbeat at %s" % ("Monitoring PID %i" % monPid, lastBeat))
            ctrlPid, lastBeat = HeartBeat("control", options.setup).status()
            lastBeat = time.ctime(lastBeat) if lastBeat else "---"
            if ctrlPid is None:
                logging.info("  %-30s  last ran at %s" % ("Controller NOT RUNNING", lastBeat))
            else:
                logging.info("  %-30s  last heartbeat at %s" % ("Controller PID %i" % ctrlPid, lastBeat))

            # Chiller
            logging.info()
            logging.info("Hardware:")
            chillerName = config["control"]["chiller"]
            if chillerName is not None:
                try:
                    chiller = config.getDevice(chillerName)
                    if chiller.getStatus(False):
                        logging.info(
                            "  %-30s  %4.1f C (target %4.1f C)"
                            % (
                                "Chiller",
                                chiller.measureTemperature(),
                                chiller.getTargetTemperature(),
                            )
                        )
                    else:
                        logging.info("  %-30s  %s" % ("Chiller", "off"))
                except Exception as e:
                    logging.info("  %-30s  %s" % ("Chiller", "powered off or not responding"))
                    logging.info(e)
            else:
                logging.info("  No chiller configured")

            # Peltier element and related sensors
            try:
                peltierPower = config.getDevice(config["control"]["peltierPower"])
                logging.info(
                    "  %-30s  %5.2f V   %5.2f A"
                    % (
                        "Peltier power supply on",
                        peltierPower.measureVoltage(),
                        peltierPower.measureCurrent(),
                    )
                )
            except Exception as e:
                logging.info("  %-30s  %s" % ("Peltier power supply", "powered off or not responding"))
                logging.info(e)
            try:
                peltierHBridge = config.getDevice(config["control"]["peltierHBridge"])
                if peltierHBridge.status() == 0:
                    logging.info("  %-30s  %s" % ("Peltier relay set for", "COOLING"))
                else:
                    logging.info("  %-30s  %s" % ("Peltier relay set for", "HEATING"))
            except Exception:
                logging.info(
                    "  WARNING: setup configured without Peltier relay,\
                       or relay not working!"
                )
            try:
                peltierSensor = config.getDevice(config["control"]["peltierSensor"])
                peltierSensor.read()
                coldplateSensor = config.getDevice(config["control"]["coldplateSensor"])
                coldplateSensor.read()
                envSensor = config.getDevice(config["control"]["envSensor"])
                envSensor.read()
                logging.info("  %-30s  %4.1f C" % ("Peltier top temperature", peltierSensor.temperature()))
                logging.info(
                    "  %-30s  %4.1f C"
                    % ("Peltier bottom temperature", coldplateSensor.temperature())
                )
                logging.info("  %-30s  %4.1f C" % ("Environment temperature", envSensor.temperature()))
                logging.info("  %-30s  %4.1f %%" % ("            humidity", envSensor.humidity()))
                logging.info(
                    "  %-30s  %4.1f C" % ("            dew point", max(envSensor.dewPoint(), -999))
                )
                logging.info(
                    "  %-30s  %4.1f C"
                    % (
                        "            dew point limit",
                        max(envSensor.dewPointLimit(), -999),
                    )
                )
            except Exception as e:
                logging.info(e)
                logging.info("  %-25s  %s" % ("Sensors", "Arduino not connected/responding"))

        # Show config info
        if options.cmd == "showconfig":
            if options.verbose:
                config.dump()
            else:
                config.summary()

        # Send commands directly to Arduino (for debugging only)
        if options.cmd == "arduino":
            arduino = config.getDevice(config["control"]["arduino"])
            if len(options.args) == 0:
                logging.error("must provide command to send to Arduino")
            else:
                cmd = " ".join(options.args)
                logging.info("sending command %s ...", cmd)
                response = arduino.sendreceive(cmd)
                logging.info(response)

        # Debugging (use -i)
        if options.cmd == "debug":
            pass

    except Exception as e:
        logging.error(str(e))
        if options.debug:
            traceback.print_exc()
