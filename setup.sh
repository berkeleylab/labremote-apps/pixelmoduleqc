# Definitions
PYVENV=qcenv

# First-time setup (if needed)
if [ ! -d labRemote ]; then
    echo 'First time setup - checking out and compiling labRemote'
    git clone --recursive https://gitlab.cern.ch/berkeleylab/labRemote.git --branch interlockUpdate
    mkdir results
    mkdir build; cd build
    cmake3 -DUSE_PYTHON=on ..
    make -j4
    cd ..
fi

# Setup Python virtual environment with labRemote plus any other required Python packages (if needed)
if [ ! -d $PYVENV ]; then
    echo "Installing Python virtual environment $PYVENV with labRemote plus any other required Python packages"
    python3 -m venv $PYVENV
    source $PYVENV/bin/activate
    python -m pip install -U pip
    # install labRemote from locally compiled version
    (cd labRemote; python -m pip install .)
    # install standard package from PyPI
    python -m pip install -r requirements.txt
fi

# Activate
source $PYVENV/bin/activate
